<?php
 
function cambridge_shipping_rules_action_info() {
 	$actions = array();
	
	$actions['cambridge_shipping_rules_action_place_order'] = array(
		'label' => t('Cambridge Shipping'),
		'group' => t('Cambridge Actions'),
		'parameter' => array(
			'order_id' => array(
				'type' => 'integer',
				'label' => t('Commerce Order ID'),
				'description' => t('Enter the ID of commerce order'),
			),
			'view_machine_name' => array(
				'type' => 'text',
				'label' => t('View Maching Name'),
				'description' => t('Enter a view maching name'),
			),
			'view_display_name' => array(
				'type' => 'text',
				'label' => t('View Display Name'),
				'description' => t('Enter a view display name'),
			),
		),
		'provides' => array(
			'response_text' => array(
				'type' => 'text',
				'label' => t('Response Text'),
			),
			'response_code' => array(
				'type' => 'integer',
				'label' => t('HTTP Response Code'),
			),
		),
	);
	
	$actions['send_shipping_edi'] = array(
		'label' => t('UK/DE send Shipping EDI'),
		'group' => t('CA Shipping'),
		'parameter' => array(
			'server' => array(
				'type' => 'text',
				'label' => t('Live or Test'),
				'description' => t('Select to use TEST or LIVE server'),
				'options list' => 'server_shipping_options',
				'restriction' => 'input',
			),
			'country' => array(
				'type' => 'text',
				'label' => t('UK or DE'),
				'description' => t('Select to use UK or DE system'),
				'options list' => 'country_shipping_options',
				'restriction' => 'input',
			),
			'order_id' => array(
				'type' => 'integer',
				'label' => t('Commerce Order ID'),
				'description' => t('Enter the ID of commerce order'),
			),
		),
		'provides' => array(
			'error' => array(
				'type' => 'text',
				'label' => t('Error Code'),
			),
			'xml' => array(
				'type' => 'text',
				'label' => t('XML document location'),
			),
		),
	);
	
	$actions['send_shipping_edi_b2b'] = array(
		'label' => t('UK/DE B2B send Shipping EDI'),
		'group' => t('CA Shipping'),
		'parameter' => array(
			'server' => array(
				'type' => 'text',
				'label' => t('Live or Test'),
				'description' => t('Select to use TEST or LIVE server'),
				'options list' => 'server_shipping_options',
				'restriction' => 'input',
			),
			'country' => array(
				'type' => 'text',
				'label' => t('UK or DE'),
				'description' => t('Select to use UK or DE system'),
				'options list' => 'country_shipping_options',
				'restriction' => 'input',
			),
			'order_id' => array(
				'type' => 'integer',
				'label' => t('Commerce Order ID'),
				'description' => t('Enter the ID of commerce order'),
			),
		),
		'provides' => array(
			'error' => array(
				'type' => 'text',
				'label' => t('Error Code'),
			),
			'xml' => array(
				'type' => 'text',
				'label' => t('XML document location'),
			),
		),
	);
	
	$actions['decode_shipment_edi'] = array(
		'label' => t('Decode Shippment EDI'),
		'group' => t('CA Shipping'),
		'parameter' => array(
			'json' => array(
				'type' => 'text',
				'label' => t('Product Data'),
			),
			'trackingNo' => array(
				'type' => 'text',
				'label' => t('Tracking Number'),
			),
			'carrier' => array(
				'type' => 'text',
				'label' => t('Carrier'),
			),
		),
		'provides' => array(
			'error' => array(
				'type' => 'text',
				'label' => t('Error Occured'),
			),
		),
	);
	
	return $actions;
}

function server_shipping_options() {
	return array(
		'test' => 'TEST',
		'live' => 'LIVE',
	);
}

function country_shipping_options() {
	return array(
		'uk' => 'UK',
		'de' => 'DE',
	);
}

function decode_shipment_edi($json, $trackingNo = NULL, $carrier = NULL) {
	//Pretending to be Admin user to get right field permissions
    global $user;
	$original_user = $user;
	$old_state = drupal_save_session();
	drupal_save_session(FALSE);
	$user = user_load(1);
	
	$productData = json_decode($json);
	
	$error = '';
	foreach($productData as &$line) {
		$line_item = commerce_line_item_load($line->line_id);
		
		if(!empty($line_item)) {
			//adding QTY shipped & serial number
			$line_item->field_qty_shipped['und'][0]['value'] = $line->qty;
			if(!empty($line->serial)) { $line_item->field_serial_number['und'][0]['value'] = $line->serial; }

			commerce_line_item_save($line_item);

			$order_id = $line_item->order_id;
		} else {
			$error .= 'failure to load ' . json_encode($line);
		}
	}
	
	if(!empty($order_id)) {
		$order = commerce_order_load($order_id);
		$order->field_tracking_number['und'][0]['value'] = !empty($trackingNo) ? $trackingNo : NULL;
		$order->field_carrier['und'][0]['value'] = !empty($carrier) ? $carrier : NULL;
		commerce_order_status_update($order, 'order_confirmed');
	}
	
	//clossing Fake admin session
    $user = $original_user;
	drupal_save_session($old_state);
	
	return array(
        'error' => !empty($error) ? $error : NULL,
    );
}

function send_shipping_edi_b2b($server, $country, $order_id) {
	$order = commerce_order_load($order_id);
	$shipment = commerce_customer_profile_load($order->commerce_customer_shipping['und'][0]['profile_id']);
	$deptor = user_load($order->uid);
	
	if(!empty($order->field_custom_order)) { $customOrder = ($order->field_custom_order['und'][0]['value'] == '1') ? TRUE : FALSE; }
	else { $customOrder = FALSE; }
	
	$doc = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><InfoLinkDocument></InfoLinkDocument>');
	
	$accessRequest = $doc->addChild('AccessRequest');
	$accessRequest->addChild('DocumentType', '62');
	$accessRequest->addChild('Version', '1.2');
	$accessRequest->addChild('EntityID', ($country == 'uk') ? '7492' : '7561');
	$accessRequest->addChild('EntityPin', ($country == 'uk') ? 'HEY7492' : 'hax7561');
	$accessRequest->addChild('TimeStamp', date('c', $order->created));
	$accessRequest->addChild('Reference1', $order->order_number);
	$accessRequest->addChild('Reference2');
	$accessRequest->addChild('Reference3');
	$accessRequest->addChild('Reference4');
	$accessRequest->addChild('Reference5');
	$accessRequest->addChild('ReplyEmailAddress', ($country == 'uk') ? 'HEYLogistics@aramex.com' : 'Frankfurt@sovereigncourier.com');
	$accessRequest->addChild('NotifyOnSuccess', '1');
	
	$firstLine = array();
	if(!empty($shipment->field_address1)) {	$firstLine[] = change_de_characters($shipment->field_address1['und'][0]['value']); }
	if(!empty($shipment->field_address2)) {	$firstLine[] = change_de_characters($shipment->field_address2['und'][0]['value']); }
	if(!empty($shipment->field_address3)) {	$firstLine[] = change_de_characters($shipment->field_address3['und'][0]['value']); }
	
	$orderInfo = $doc->addChild('Order');
	$orderInfo->addChild('CustomerNumber', ($country == 'uk') ? '6376' : '150892243');
	$orderInfo->addChild('CustomerEntity', ($country == 'uk') ? 'HEY' : 'HAX');
	$orderInfo->addChild('SiteCode', ($country == 'uk') ? 'HEY' : 'SOVWH');
	$orderInfo->addChild('OrderNumber', $order->order_number);
	$orderInfo->addChild('SOASNDate', date('c', $order->created));
	$orderInfo->addChild('ConsigneeName', change_de_characters($shipment->field_cntcprsn['und'][0]['value']));
	$orderInfo->addChild('ConsigneeAddress', change_de_characters($deptor->field_custname['und'][0]['value']) . ', ' . implode(', ', $firstLine));
	$orderInfo->addChild('ConsigneeZipCode', $shipment->field_zip['und'][0]['value']);
	$orderInfo->addChild('ConsigneeReference', $order->mail);
	$orderInfo->addChild('ConsigneeAttention', change_de_characters($shipment->field_cntcprsn['und'][0]['value']));
	$orderInfo->addChild('ConsigneeCity', change_de_characters($shipment->field_city['und'][0]['value']));
	$orderInfo->addChild('ConsigneeCountryCode', $shipment->field_country['und'][0]['iso2']);
	$orderInfo->addChild('ConsigneeState', !empty($shipment->field_state) ? change_de_characters($shipment->field_state['und'][0]['value']) : NULL);
	$orderInfo->addChild('ConsigneePhone', !empty($shipment->field_phone1) ? change_de_characters($shipment->field_phone1['und'][0]['value']) : NULL);
	$orderInfo->addChild('Comments');
	$orderInfo->addChild('HandlingType', 'Normal');
	$orderInfo->addChild('Carrier', 'Courier');
	$orderInfo->addChild('CustomerReference', 'APPLC');
	$orderInfo->addChild('UserDefined1', $order->order_number);
	$orderInfo->addChild('UserDefined2', 'Courier');
	$orderInfo->addChild('UserDefined3', 'ftp://b2b_ftp.audiopartnership.com/de');
	
	foreach($order->commerce_line_items['und'] as &$line) {
		$line_item = commerce_line_item_load($line['line_item_id']);
		if($line_item->type == 'product') {
			$product = commerce_product_load($line_item->commerce_product['und'][0]['product_id']);
			$lines = $orderInfo->addChild('OrderItem');
			$lines->addChild('SKU', $product->sku);
			$lines->addChild('Quantity', round($line_item->quantity));
			$lines->addChild('AutoAllocate', '0');
			$lines->addChild('Comments', !empty($product->field_comment) ? $product->field_comment['und'][0]['value'] : NULL);
			$lines->addChild('Reference1', $line_item->line_item_id);
		}
	}
	
	$request = ($server == 'live') ? 'https://infolink.aramex.net/post.aspx' : 'https://infolink.dev.aramex.net/post.aspx';

    $postOptions = array(
        'method' => 'POST',
        'timeout' => 30,
        'data' => $doc->asXml(),
        'max_redirects' => 3,
        'headers' => array(
            'Content-Type' => 'text/xml; charset=utf-8',
            'Authorization' => 'Basic '. drupal_base64_encode('AudioPartnerShip:AP@123123'),
        ),
    );

    $postResult = drupal_http_request($request, $postOptions);
	
	if(!empty($postResult->error)) {
		$error = failure($country, $order, $doc->asXml());
	}
	
	return array(
        'error' => !empty($postResult->error) ? $postResult->error : NULL,
		'xml' => !empty($error['order_xml']) ? $error['order_xml'] : NULL,
    );
}

function send_shipping_edi($server, $country, $order_id) {
	$order = commerce_order_load($order_id);
	$shipment = commerce_customer_profile_load($order->commerce_customer_shipping['und'][0]['profile_id']);
	
	$customOrder = FALSE;
	if(!empty($order->field_custom_order)) { $customOrder = ($order->field_custom_order['und'][0]['value'] == '1') ? TRUE : FALSE; }
	
	$doc = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><InfoLinkDocument></InfoLinkDocument>');
	
	$accessRequest = $doc->addChild('AccessRequest');
	$accessRequest->addChild('DocumentType', '62');
	$accessRequest->addChild('Version', '1.2');
	$accessRequest->addChild('EntityID', ($country == 'uk') ? '7492' : '7561');
	$accessRequest->addChild('EntityPin', ($country == 'uk') ? 'HEY7492' : 'hax7561');
	$accessRequest->addChild('TimeStamp', date('c', $order->created));
	$accessRequest->addChild('Reference1', $order->order_number);
	$accessRequest->addChild('Reference2');
	$accessRequest->addChild('Reference3');
	$accessRequest->addChild('Reference4');
	$accessRequest->addChild('Reference5');
	$accessRequest->addChild('ReplyEmailAddress', ($country == 'uk') ? 'HEYLogistics@aramex.com' : 'Frankfurt@sovereigncourier.com');
	$accessRequest->addChild('NotifyOnSuccess', '1');
	
	$address = $shipment->commerce_customer_address['und'][0];
	
	$firstLine = [];
	if(!empty($address['sub_premise'])) { $firstLine[] = change_de_characters($address['sub_premise']); }
	if(!empty($address['premise'])) { $firstLine[] = change_de_characters($address['premise']); }
	if(!empty($address['thoroughfare'])) { $firstLine[] = change_de_characters($address['thoroughfare']); }
	if(!empty($address['dependent_locality'])) { $firstLine[] = change_de_characters($address['dependent_locality']); }
	
	$orderInfo = $doc->addChild('Order');
	$orderInfo->addChild('CustomerNumber', ($country == 'uk') ? '6376' : '150892243');
	$orderInfo->addChild('CustomerEntity', ($country == 'uk') ? 'HEY' : 'HAX');
	$orderInfo->addChild('SiteCode', ($country == 'uk') ? 'HEY' : 'SOVWH');
	$orderInfo->addChild('OrderNumber', $order->order_number);
	$orderInfo->addChild('SOASNDate', date('c', $order->created));
	$orderInfo->addChild('ConsigneeName', change_de_characters($address['name_line']));
	$orderInfo->addChild('ConsigneeAddress', implode(', ', $firstLine));
	$orderInfo->addChild('ConsigneeZipCode', $address['postal_code']);
	$orderInfo->addChild('ConsigneeReference', $order->mail);
	$orderInfo->addChild('ConsigneeAttention', change_de_characters($address['name_line']));
	$orderInfo->addChild('ConsigneeCity', change_de_characters($address['locality']));
	$orderInfo->addChild('ConsigneeCountryCode', $address['country']);
	$orderInfo->addChild('ConsigneeState', !empty($address['administrative_area']) ? change_de_characters($address['administrative_area']) : NULL);
	$orderInfo->addChild('ConsigneePhone', !empty($shipment->field_contact_number) ? change_de_characters($shipment->field_contact_number['und'][0]['value']) : NULL);
	
	if(!empty($shipment->field_delivery_instructions)) {
		$orderInfo->addChild('Comments', empty($shipment->field_delivery_instructions) ? NULL : change_de_characters($shipment->field_delivery_instructions['und'][0]['value']));
	} else {
		$orderInfo->addChild('Comments');
	}
	
	$orderInfo->addChild('HandlingType', 'Normal');
	$orderInfo->addChild('Carrier', 'Courier');
	$orderInfo->addChild('CustomerReference', 'APPLC');
	$orderInfo->addChild('UserDefined1', ($customOrder == TRUE) ? $order->field_custom_order_number['und'][0]['value'] : $order->order_number);
	$orderInfo->addChild('UserDefined2', 'Courier');
	$orderInfo->addChild('UserDefined3', ($country == 'uk') ? 'ftp://b2c_ftp.audiopartnership.com/uk' : 'ftp://b2c_ftp.audiopartnership.com/de');
	
	foreach($order->commerce_line_items['und'] as &$line) {
		$line_item = commerce_line_item_load($line['line_item_id']);
		if($line_item->type == 'product') {
			$product = commerce_product_load($line_item->commerce_product['und'][0]['product_id']);
			$lines = $orderInfo->addChild('OrderItem');
			$lines->addChild('SKU', $product->sku);
			$lines->addChild('Quantity', round($line_item->quantity));
			$lines->addChild('AutoAllocate', '0');
			$lines->addChild('Comments', !empty($product->field_comment) ? $product->field_comment['und'][0]['value'] : NULL);
			$lines->addChild('Reference1', $line_item->line_item_id);
		}
	}
	
	$request = ($server == 'live') ? 'https://infolink.aramex.net/post.aspx' : 'https://infolink.dev.aramex.net/post.aspx';

    $postOptions = array(
        'method' => 'POST',
        'timeout' => 30,
        'data' => $doc->asXml(),
        'max_redirects' => 3,
        'headers' => array(
            'Content-Type' => 'text/xml; charset=utf-8',
            'Authorization' => 'Basic '. drupal_base64_encode('AudioPartnerShip:AP@123123'),
        ),
    );

    $postResult = drupal_http_request($request, $postOptions);
	
	if(!empty($postResult->error)) {
		$error = failure($country, $order, $doc->asXml());
	}
	
	return array(
        'error' => !empty($postResult->error) ? $postResult->error : NULL,
		'xml' => !empty($error['order_xml']) ? $error['order_xml'] : NULL,
    );
}

function failure($country, $order, $xml) {
	global $user;
	$original_user = $user;
	$old_state = drupal_save_session();
	drupal_save_session(FALSE);
	$user = user_load(1);

	$order_xml = 'private://shipment_xml/' . strtoupper($country) . '_SHIP_XML_' . $order->order_number . '_' . date('YmdHis'). '.xml';
	file_unmanaged_save_data($xml, $order_xml);

	$user = $original_user;
	drupal_save_session($old_state);
	
	return array(
		'order_xml' => $order_xml,
	);
}

function change_de_characters($string) {
	$from = array('ß','æ','ö','Ö','è','È','é','É','Ü','ü','Ä','ä');
	$to = array('ss','ae','oe','Oe','e','E','e','E','Ue','ue','Ae','ae');
	return htmlspecialchars(str_replace($from, $to, $string));
}