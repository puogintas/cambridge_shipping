<?php

// 300 seconds = 5 minutes script can run
ini_set('max_execution_time', 300);

//Ftp settings
$ftp = array(
    'server' => 'b2b_ftp.audiopartnership.com',
    'name' => 'B2B',
    'pass' => '!!NewPassword001',
    'dir' => 'DE',
);

//Database settings
$db = array(
    'host' => 'localhost',
    'username' => 'ca_b2b_de_shadow',
    'password' => '1V7eLlZc4CeGmFSahACv',
    'name' => 'ca_b2b_de_shadow',
    'table' => 'Shipment_EDI',
);

//Require access token to trigger this php script
if (empty($_GET['access_token']) or $_GET['access_token'] != '7cnMTkDof3fTu70uB9gojdTzRtnrWB9C') {
	echo 'Access denied.';
	exit();
}

//Connecting to database
$mysqli = new mysqli($db['host'], $db['username'], $db['password'], $db['name']);

if ($mysqli->connect_errno) {
	echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	exit();
}

$db_fields = '';
$db_prepare_values = '';
$db_types = '';

$db_update_duplicate = true;
$db_duplicate_unique_key = 'Order_Number';
$db_duplicate_sql = ' ON DUPLICATE KEY UPDATE ';

$connect = ftp_connect($ftp['server']);
$login_result = ftp_login($connect, $ftp['name'], $ftp['pass']);
$contents = ftp_nlist($connect, $ftp['dir']);

if(count($contents) != 1 && $contents[0] != 'de/read') {
	
	$totals = array(
		'success' => 0,
		'failed' => 0,
	);

	$mysqli->query("START TRANSACTION");

	$i = 1;

	foreach($contents as &$filename) {
		if(endsWith($filename, '.XML')) {
			$currentfile = 'ftp://' . $ftp['name'] . ':' . $ftp['pass'] . '@' . $ftp['server'] . '/'  .  $ftp['dir'] . '/' . $filename;
			$handle = fopen($currentfile, 'r');
			$content = fread($handle, filesize($currentfile));
			fclose($handle);

			//reading data and converting to array
			$data = simplexml_load_string($content);
			$json = json_encode($data);
			$array = json_decode($json,TRUE);

			$items = array();
			$itemXML = $array['OrderUpdate']['OrderDetails']['OrderItem'];
			if(!empty($itemXML[0])) {
				foreach($itemXML as &$item) {
					$itemArray = array(
						'sku' => (!empty($item['SKU'])) ? $item['SKU'] : '0',
						'qty' => (!empty($item['ActualQty'])) ? $item['ActualQty'] : NULL,
						'line_id' => (!empty($item['Reference1'])) ? $item['Reference1'] : NULL,
						'serial' => (!empty($item['Reference2'])) ? $item['Reference2'] : NULL,
					);
					$items[] = $itemArray;
				}
			} else {
				$itemArray = array(
					'sku' => (!empty($itemXML['SKU'])) ? $itemXML['SKU'] : NULL,
					'qty' => (!empty($itemXML['ActualQty'])) ? $itemXML['ActualQty'] : NULL,
					'line_id' => (!empty($itemXML['Reference1'])) ? $itemXML['Reference1'] : NULL,
					'serial' => (!empty($itemXML['Reference2'])) ? $itemXML['Reference2'] : NULL,
				);
				$items[] = $itemArray;
			}

			$db_map = array();
			// map database fields to values
			$db_map['Order_Number'] = !empty($array['OrderUpdate']['OrderDetails']['OrderNo']) ? $array['OrderUpdate']['OrderDetails']['OrderNo'] : '';
			$db_map['TrackingNO'] = !empty($array['OrderUpdate']['Comment1']) ? $array['OrderUpdate']['Comment1'] : 'No Tracking number';
			$db_map['Carrier'] = !empty($array['OrderUpdate']['OrderDetails']['Carrier']) ? $array['OrderUpdate']['OrderDetails']['Carrier'] : 'No Carrier';
			$db_map['ItemsJSON'] = !empty($items) ? json_encode($items) : '';
			$db_map['Filename'] = !empty($filename) ? $filename : '';

			// output db map
			print("<pre>" . print_r($db_map, true). "</pre>");

			$bind_params = array();

			// get db field name from map and prepare sql statement
			foreach ($db_map as $field_name => $value) {
				if ($i == 1) {
					$db_fields .= $field_name.', ';
					$db_prepare_values .= '?, ';
					$db_types .= 's';

					if ($db_duplicate_unique_key != $field_name) {
						$db_duplicate_sql .= $field_name . ' = VALUES(' . $field_name . '), ';
					}
				}

				$bind_params[] = $value;
			}

			array_unshift($bind_params, $db_types);

			$db_fields = rtrim($db_fields, ", ");
			$db_prepare_values = rtrim($db_prepare_values, ", ");
			$db_duplicate_sql = rtrim($db_duplicate_sql, " ,");

			$query = 'INSERT INTO ' . $db['table'] . ' (' . $db_fields . ') VALUES (' . $db_prepare_values . ')';
			$query .= ($db_update_duplicate) ? $db_duplicate_sql : '';

			$stmt = $mysqli->prepare($query);

			call_user_func_array(array($stmt, 'bind_param'), ref_values($bind_params));

			if ($stmt->execute()) {
				$totals['success']++;
			} else {
				$totals['failed']++;
			}

			$i++;

			try {
				$readFile = 'ftp://' . $ftp['name'] . ':' . $ftp['pass'] . '@' . $ftp['server'] . '/' .  $ftp['dir'] . '/read/' . $filename;
				$options = array('ftp' => array('overwrite' => true));
				$stream = stream_context_create($options);
				file_put_contents($readFile, $content, 0, $stream);
				unlink($currentfile);
			}
			catch (Exception $e) { print_r($e); }
		}
	}


	$stmt->close();
	$mysqli->query("COMMIT");

	echo 'Customers Fetched: ' . count($contents) . '<br>';
	echo 'DB Rows Inserted: ' . $totals['success'] . '<br>';
	echo 'DB Rows Failed: ' . $totals['failed'] . '<br>';
	
} else {
	echo 'Directory is empty';
}

function ref_values($arr) {
    if (strnatcmp(phpversion(),'5.3') >= 0) {
        $refs = array();
        foreach($arr as $key => $value) {
            $refs[$key] = &$arr[$key];
        }
        return $refs;
    }
    return $arr;
}

function endsWith($haystack, $needle) {
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}

?>